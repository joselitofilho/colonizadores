﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatanWPF.br.com.catan.protocolo
{
    /// <summary>
    /// 
    /// </summary>
    public class GenericObject
    {
        #region Propriedades
        /// <summary>
        /// 
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public object data { get; set; }
        #endregion
    }
}
