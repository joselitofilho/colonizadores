﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using CatanWPF.br.com.catan.lang;
using CatanWPF.br.com.catan.protocol.request;
using CatanWPF.br.com.catan.protocol.response;
using CatanWPF.br.com.catan.util;

namespace CatanWPF.br.com.catan.view
{
    /// <summary>
    /// Interaction logic for LoginTela.xaml
    /// </summary>
    public partial class LoginTela : UserControl
    {
        public LoginTela()
        {
            InitializeComponent();

            // Atualizando o contexto com a requisição do login.
            DataContext = new LoginRequest();
        }

        /// <summary>
        /// 
        /// </summary>
        private void fazerLogin()
        {
            try
            {
                // Desabilita o botão de login.
                btLogin.IsEnabled = false;

                // Cria a requisição de login
                var login = DataContext as LoginRequest;
                if (login != null)
                {
                    // Alterando o título da janela principal.
                    var mainWindow = UIUtil.FindVisualParent<Window>(this);
                    if (mainWindow != null)
                    {
                        mainWindow.Dispatcher.Invoke(
                            DispatcherPriority.Normal,
                            new Action(
                                delegate
                                    {
                                        mainWindow.Title = "Colonizador " + login.nick;
                                    }));
                    }

                    Session.getSession().Add(Session.KEY_NICK, login.nick);

                    // Envia a requisição.
                    MainManager.Communication.SendObject(login);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        public void processResponse(int status)
        {
            MessageBox.Show("Login error: " + status,
                            "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            // Habilita o botão de login.
            btLogin.IsEnabled = true;
        }

        private void btLogin_Click(object sender, RoutedEventArgs e)
        {
            fazerLogin();
        }
    }
}
