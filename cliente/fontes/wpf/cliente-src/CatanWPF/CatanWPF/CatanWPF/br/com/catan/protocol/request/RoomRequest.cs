﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatanWPF.br.com.catan.protocol.request
{
    public class RoomRequest
    {
        public enum EActions
        {
            Enter = 0,
            Create = 1
        }

        public int action { get; set; }
    }
}
