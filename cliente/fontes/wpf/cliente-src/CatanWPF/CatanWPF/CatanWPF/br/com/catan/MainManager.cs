﻿/**
 * \name    MainManager.cs
 * \author  Joselito Viveiros Nogueira Filho - joselitofilhoo@gmail.com
 * \date    12/11/2012
 */

using System;
using System.Linq;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Threading;
using CatanWPF.br.com.catan.communication;
using CatanWPF.br.com.catan.lang;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CatanWPF.br.com.catan.protocol.process;

namespace CatanWPF.br.com.catan
{
    /// <summary>
    /// 
    /// </summary>
    public class MainManager : /*implements*/IObserver
    {
        #region Constantes
        public const string ROOM_VIEW_NAME = "RoomView";
        #endregion

        #region Construtores

        /// <summary>
        /// Manager client's main.
        /// </summary>
        /// <param name="mainWindow_"></param>
        public MainManager(MainWindow mainWindow_)
        {
            // Instance the configuration.
            configuration = Configuration.getConfiguration();

            try
            {
                // Instance of the communication.
                Communication = new CommunicationInterface(configuration.Communication.Ip,
                                                           configuration.Communication.Port);
                // Instance of the session.
                session = Session.getSession();

                // Reference main control.
                mainWindow = mainWindow_;

                // Adding the class itself MainManager(Observer) in Communication(Subject).
                Communication.Attach(this);
            }
            catch (SocketException)
            {
                MessageBox.Show("Não foi possível estabelecer uma conexão com o servidor. Tente mais tarde.",
                                "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
        }

        #endregion

        #region Métodos

        /**
         * \overload void IObserver::Update()
         */

        public void Update()
        {
            processResponse(Communication.SubjectState);
        }

        /// <summary>
        /// Processes the data received from the server.
        /// </summary>
        /// <param name="json">data in json format.</param>
        private void processResponse(string json)
        {
            var jObj = JObject.Parse(json);

            // Searching for the type of response.
            string type = jObj.Property("type").Value.ToString();

            // Searching for the data.
            JToken jTk = jObj.Property("data").Value;

            // Type: Login
            if (type.Equals(LoginProcess.TYPE))
            {
                new LoginProcess(ref mainWindow, jTk).execute();
            }
            else if (type.Equals(RoomProcess.TYPE))
            {
                new RoomProcess(ref mainWindow, jTk).execute();
            }

        }
        #endregion

        #region Atributos

        /// <summary>
        /// 
        /// </summary>
        private readonly Configuration configuration;

        private Session session;
        private MainWindow mainWindow;

        /// <summary>
        /// Interface de comunicação com o servidor. Ela será utilizada em toda a aplicação.
        /// </summary>
        public static CommunicationInterface Communication { get; set; }

        #endregion
    }
}