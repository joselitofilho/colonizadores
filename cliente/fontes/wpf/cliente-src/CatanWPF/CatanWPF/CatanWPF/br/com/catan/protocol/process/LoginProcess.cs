﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using CatanWPF.br.com.catan.protocol.response;
using CatanWPF.br.com.catan.view;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CatanWPF.br.com.catan.protocol.process
{
    public class LoginProcess
    {
        public const string TYPE = "LoginResponse";
        private MainWindow mainWindow;
        private JToken jTk;

        public LoginProcess(ref MainWindow mainWindow, Newtonsoft.Json.Linq.JToken jTk)
        {
            this.mainWindow = mainWindow;
            this.jTk = jTk;
        }

        public void execute()
        {
            var jsonSerializer = new JsonSerializer();
            var loginResp = (LoginResponse) jsonSerializer.Deserialize(
                new JTokenReader(jTk),
                typeof (LoginResponse));

            if (loginResp.status == (int)ResponseStatus.SUCCESS)
            {
                loginSucess();
            }
            else
            {
                loginError(loginResp.status);
            }
        }

        private void loginSucess()
        {
            mainWindow.GridMain.Dispatcher.Invoke(
                DispatcherPriority.Normal,
                new Action(
                    delegate
                        {
                            // Removendo tela de login.
                            mainWindow.GridMain.Children.Remove(mainWindow.LoginView);

                            // Adicionando tela de salas de espera.
                            var roomView = new RoomView
                                               {
                                                   Name = MainWindow.ROOM_VIEW_NAME
                                               };

                            mainWindow.GridMain.Children.Add(roomView);

                            roomView.enterTheRoom();
                        }
                    ));
        }

        private void loginError(int status)
        {
            // Processa a resposta para a tela de login.
            mainWindow.LoginView.processResponse(status);
        }
    }
}
