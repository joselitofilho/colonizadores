﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using CatanWPF.br.com.catan.model;
using CatanWPF.br.com.catan.protocol.response;
using CatanWPF.br.com.catan.util;
using CatanWPF.br.com.catan.view;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CatanWPF.br.com.catan.protocol.process
{
    public class RoomProcess
    {
        public const string TYPE = "RoomResponse";
        private MainWindow mainWindow;
        private JToken jTk;

        public RoomProcess(ref MainWindow mainWindow, JToken jTk)
        {
            this.mainWindow = mainWindow;
            this.jTk = jTk;
        }

        public void execute()
        {
            var jsonSerializer = new JsonSerializer();
            var roomResp = (RoomResponse)jsonSerializer.Deserialize(
                new JTokenReader(jTk),
                typeof(RoomResponse));

            var players = new List<Player>();

            if (roomResp.player1 != null) players.Add(roomResp.player1);
            if (roomResp.player2 != null) players.Add(roomResp.player2);
            if (roomResp.player3 != null) players.Add(roomResp.player3);
            if (roomResp.player4 != null) players.Add(roomResp.player4);

            if (players.Count > 0)
            {
                newPlayerEnteredTheRoom(players);
            }
            else
            {
                roomError();
            }
        }

        private void newPlayerEnteredTheRoom(List<Player> players)
        {
            mainWindow.GridMain.Dispatcher.Invoke(
                DispatcherPriority.Normal,
                new Action(
                    delegate
                    {
                        var roomView = UIUtil.FindChild<RoomView>(mainWindow.GridMain, MainWindow.ROOM_VIEW_NAME);
                        roomView.newPlayerEnteredTheRoom(players);
                    }
                    ));
        }

        private void roomError()
        {
            throw new NotImplementedException();
        }
    }
}
