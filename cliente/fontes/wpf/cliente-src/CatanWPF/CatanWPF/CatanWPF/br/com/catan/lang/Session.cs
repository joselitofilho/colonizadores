﻿using System;
using System.Collections.Generic;

namespace CatanWPF.br.com.catan.lang
{
    /// <summary>
    /// Singleton.
    /// </summary>
    public class Session : Dictionary<object, object>
    {
        #region Constantes
        public const string KEY_NICK = "nick";
        #endregion

        #region Contrutores
        /// <summary>
        /// Construtor padrão.
        /// </summary>
        private Session()
        {
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Verifica se existe alguma instância de Session. Se não existir
        /// ela é criada e retornada. Caso contrário, ela apenas é retornada.
        /// </summary>
        /// <returns>Singleton da classe Session.</returns>
        public static Session getSession()
        {
            if (session == null)
            {
                session = new Session();
            }

            return session;
        }
        #endregion

        #region Atributos
        /// <summary>
        /// Singleton desta classe.
        /// </summary>
        private static Session session;

        #endregion

        #region Propriedades
        #endregion
    }
}
