﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CatanWPF.br.com.catan.model;

namespace CatanWPF.br.com.catan.protocol.response
{
    public class RoomResponse
    {
        public Player player1 { get; set; }
        public Player player2 { get; set; }
        public Player player3 { get; set; }
        public Player player4 { get; set; }
    }
}
