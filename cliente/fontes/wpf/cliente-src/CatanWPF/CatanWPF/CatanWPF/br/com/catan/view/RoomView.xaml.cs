﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CatanWPF.br.com.catan.lang;
using CatanWPF.br.com.catan.model;
using CatanWPF.br.com.catan.protocol.request;

namespace CatanWPF.br.com.catan.view
{
    /// <summary>
    /// Interaction logic for RoomView.xaml
    /// </summary>
    public partial class RoomView : UserControl
    {
        public RoomView()
        {
            InitializeComponent();
        }

        public void enterTheRoom()
        {
            try
            {
                // Cria a requisição de login.
                var room = new RoomRequest
                               {
                                   action = (int)RoomRequest.EActions.Enter
                               };

                // Envia a requisição.
                MainManager.Communication.SendObject(room);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="players"></param>
        public void newPlayerEnteredTheRoom(List<Player> players)
        {
            var nickSession = Session.getSession()[Session.KEY_NICK] as string;
            int position = 1;

            foreach (var player in players)
            {
                switch (position)
                {
                    case 1:
                        jogador1.Text = player.nick;
                        break;
                    case 2:
                        jogador2.Text = player.nick;
                        break;
                    case 3:
                        jogador3.Text = player.nick;
                        break;
                    case 4:
                        jogador4.Text = player.nick;
                        break;
                }

                ++position;
            }

            // Habilitando ou desabilitando o botão de iniciar a partida. O botão deve aparecer
            // apenas para o jogador 1. 
            if (nickSession != null && players[0].nick == nickSession)
            {
                btnIniciar.Visibility = Visibility.Visible;
            }
            else
            {
                btnIniciar.Visibility = Visibility.Hidden;
            }
        }   

        private void btnIniciar_Click(object sender, RoutedEventArgs e)
        {
            iniciarJogo();
        }

        private void iniciarJogo()
        {
            try
            {
                // Cria a requisição de login.
                var room = new RoomRequest
                {
                    action = (int)RoomRequest.EActions.Create
                };

                // Envia a requisição.
                MainManager.Communication.SendObject(room);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
