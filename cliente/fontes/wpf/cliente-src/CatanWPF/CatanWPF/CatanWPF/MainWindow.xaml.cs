﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CatanWPF.br.com.catan;

namespace CatanWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string ROOM_VIEW_NAME = "RoomView";

        public MainWindow()
        {
            InitializeComponent();
            //
            _mainManager = new MainManager(this);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            // TODO: Enviar mensagem de deslogar.

            // TODO: Refletir sobre isso...
            // Mata a aplicação pois as threads que eram criadas no decorrer da aplicação
            // não estavam sendo fechadas quando a aplicação fechava normalmente.
            Process.GetCurrentProcess().Kill();
        }

        private MainManager _mainManager;
    }
}
