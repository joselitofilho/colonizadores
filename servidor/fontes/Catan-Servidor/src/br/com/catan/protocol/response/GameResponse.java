package br.com.catan.protocol.response;

import java.util.ArrayList;

import br.com.catan.model.Hexagon;
import br.com.catan.model.Player;

public class GameResponse extends AbstractResponse {

	/**
	 * Create the response of game protocol
	 */
	public GameResponse() {

		super(GameResponse.class.getSimpleName());

	}
	
	public void setPlayer1(Player player) {

		data.put("player1", player);

	}

	public void setPlayer2(Player player) {

		data.put("player2", player);

	}

	public void setPlayer3(Player player) {

		data.put("player3", player);

	}

	public void setPlayer4(Player player) {

		data.put("player4", player);

	}
	
	public void setHexagons(ArrayList<Hexagon> hexagons) {

		data.put("hexagons", hexagons);

	}

	public void setPlayerTurn(Player playerTurn) {
		
		data.put("playerTurn", playerTurn);
		
	}
}
