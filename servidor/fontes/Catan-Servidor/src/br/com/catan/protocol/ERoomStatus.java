package br.com.catan.protocol;

public enum ERoomStatus {

	ENTER(0),
	CREATE(1);
	
	private int value;
	
	private ERoomStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
