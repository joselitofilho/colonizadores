package br.com.catan.protocol.request;

public class LoginRequest {

	public static final Object TYPE_NAME = "LoginRequest";
	
	private String nick;

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
}
