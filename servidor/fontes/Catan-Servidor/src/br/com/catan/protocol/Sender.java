package br.com.catan.protocol;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Joselito Viveiros Nogueira Filho
 * 
 */
public class Sender {

	// List of sockets.
	private List<Socket> _sockets;

	/**
	 * Constructor that takes a list of sockets that will be used during
	 * processing.
	 * 
	 * @param socket
	 */
	public Sender(Socket socket) {
		_sockets = new ArrayList<Socket>();
		_sockets.add(socket);
	}

	/**
	 * Constructor that takes a socket that will be used during processing.
	 * 
	 * @param socket
	 */
	public Sender(List<Socket> sockets) {
		_sockets = sockets;
	}

	/**
	 * Send message to list of socket.
	 * 
	 * @param message
	 */
	public void sendMessage(String message) {
		if (_sockets != null) {
			for (Socket socket : _sockets) {
				try {
					PrintStream printStream = new PrintStream(
							socket.getOutputStream());
					printStream.print(message);
					printStream.flush();

					Logger.getLogger(Sender.class.getName()).log(Level.INFO,
							"Send data: " + message);
				} catch (IOException e) {
					// Log the exception
					Logger.getLogger(Sender.class.getName()).log(Level.SEVERE,
							e.getMessage());
				}
			}
		}
	}
}
