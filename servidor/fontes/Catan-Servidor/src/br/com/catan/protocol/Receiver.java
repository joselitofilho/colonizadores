package br.com.catan.protocol;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import br.com.catan.protocol.process.LoginProcess;
import br.com.catan.protocol.process.RoomProcess;
import br.com.catan.protocol.request.LoginRequest;
import br.com.catan.protocol.request.RoomRequest;

/**
 * Class that receive the socket and treat the data, forwarding to the target
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 */
public class Receiver extends Thread {

	// Sentinel variable to while block
	private boolean execute;

	// Socket with client
	private Socket socket;

	// Logger class to log the actions
	private Logger logger;

	// Object mapper of JSON
	private ObjectMapper mapper;

	//
	private List<Server> list = new ArrayList<Server>();

	//
	private String stateMessage;

	//
	private List<Socket> stateSockets;

	public Socket getSocket() {
		return socket;
	}

	public String getStateMessage() {
		return stateMessage;
	}

	public void setStateMessage(String stateMessage) {
		this.stateMessage = stateMessage;
	}

	public List<Socket> getStateSockets() {
		return stateSockets;
	}

	public void setStateSockets(List<Socket> stateSockets) {
		this.stateSockets = stateSockets;
	}

	/**
	 * This class will receiver the socket data and forward to respective target
	 * 
	 * @param Client
	 *            socket
	 */
	public Receiver(Socket socket) {

		// Preparing the battlefield :-)
		logger = Logger.getLogger(Receiver.class.getName());
		execute = true;
		this.socket = socket;
		logger.log(Level.INFO, "Receiver initialized...");

	}

	/**
	 * Read the socket data and send to specific target
	 */
	public void run() {

		try {

			// Preparing the DataInputStrean to read the socket data
			DataInputStream in = new DataInputStream(socket.getInputStream());
			byte[] data;
			int bufferLength;

			/*
			 * While the execute sentinel varibale is true execute the block
			 * bellow
			 */
			while (execute) {

				// Buffer length of data
				bufferLength = in.available();

				// Byte array of data
				data = new byte[bufferLength];

				/*
				 * If bufferLength is major then 0 read the data and process it
				 */
				if (bufferLength > 0) {

					// Read the data and send to the processData
					in.read(data);
					processData(new String(data));

				}

			}

		} catch (IOException e) {

			// Register the error on the log
			logger.log(Level.SEVERE, "ERROR: " + e.getMessage());

		}

	}

	/**
	 * Process the data receives of the client socket
	 * 
	 * @param json
	 *            data of the client
	 */
	private void processData(String json) {

		try {

			// Register the data received of client on the log and create the
			// object mapper of JSON
			logger.log(Level.INFO, "Receive data: " + json);
			mapper = new ObjectMapper();

			// Read the type of data was received
			String type = mapper.readTree(json).path("type").asText();

			/*
			 * Decide who must respond the type received
			 */
			if (type.equals(LoginRequest.TYPE_NAME)) {
				LoginRequest l = mapper.readValue(
						mapper.readTree(json).path("data"), LoginRequest.class);

				new LoginProcess(l, socket).execute();
			} else if (type.equals(RoomRequest.TYPE_NAME)) {
				RoomRequest r = mapper.readValue(
						mapper.readTree(json).path("data"), RoomRequest.class);
				new RoomProcess(r, socket).execute();
			}
			/*
			 * else if (type.equals(RoomChangeRequest.TYPE_NAME)) {
			 * RoomChangeRequest roomChangeRequest = mapper.readValue(mapper
			 * .readTree(json).path("data"), RoomChangeRequest.class);
			 * 
			 * new ProcessRoomChange(roomChangeRequest, socket).execute(); }
			 * else if (type.equals(StartGameRequest.TYPE_NAME)) {
			 * StartGameRequest startGameRequest = mapper.readValue(mapper
			 * .readTree(json).path("data"), StartGameRequest.class);
			 * 
			 * new ProcessStartGame(startGameRequest, socket).execute(); } else
			 * if (type.equals(PlaceArmyRequest.TYPE_NAME)) { PlaceArmyRequest
			 * placeArmyRequest = mapper.readValue(mapper
			 * .readTree(json).path("data"), PlaceArmyRequest.class);
			 * 
			 * new ProcessPlaceArmy(placeArmyRequest, socket).execute(); }
			 */

		} catch (JsonProcessingException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();

			logger.log(Level.SEVERE, "ERROR: " + e.getMessage());

		} catch (IOException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();

			logger.log(Level.SEVERE, "ERROR: " + e.getMessage());

		}

	}
}
