package br.com.catan.protocol.process;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import br.com.catan.model.Game;
import br.com.catan.model.Hexagon;
import br.com.catan.model.Player;
import br.com.catan.model.Room;
import br.com.catan.model.UsersTable;
import br.com.catan.protocol.ERoomStatus;
import br.com.catan.protocol.Sender;
import br.com.catan.protocol.request.RoomRequest;
import br.com.catan.protocol.response.GameResponse;
import br.com.catan.protocol.response.RoomResponse;
import br.com.catan.util.Session;

public class RoomProcess {

	private RoomRequest roomRequest;
	private Socket socket;

	public RoomProcess(RoomRequest r, Socket socket) {
		this.roomRequest = r;
		this.socket = socket;
	}

	public void execute() {

		if (roomRequest.getAction() == ERoomStatus.ENTER.getValue()) {
			executeEnterTheRoom();
		} else if (roomRequest.getAction() == ERoomStatus.CREATE.getValue()) {
			startGame();
		}
	}

	private void executeEnterTheRoom() {
		try {
			Session session = Session.getInstance();

			// Recuperando a tabela de usuários.
			UsersTable users = (UsersTable) session
					.get(Session.KEY_USERS_LOGGED);

			// Recuperando o nick correspondente ao socket.
			String nick = users.getNick(socket);

			// Recuperando a sala da sessão.
			Room room = (Room) session.get(Session.KEY_ROOM);

			// Verificando integridade da sala.
			if (room == null) {
				room = new Room();
			}

			// Adiciona o jogador na próxima posição se possível.
			room.adicionarJogador(nick);
			
			// NOTA: Se não conseguir adicionar o jogador na sala, a resposta irá 
			//       apenas com os jogadores que estão dentro dela.

			RoomResponse roomResponse = new RoomResponse();
			Player player1 = room.getJogador1();
			Player player2 = room.getJogador2();
			Player player3 = room.getJogador3();
			Player player4 = room.getJogador4();

			if (player1 != null)
				roomResponse.setPlayer1(player1);
			if (player2 != null)
				roomResponse.setPlayer2(player2);
			if (player3 != null)
				roomResponse.setPlayer3(player3);
			if (player4 != null)
				roomResponse.setPlayer4(player4);

			ArrayList<String> nicks = room.playersNick();
			ArrayList<Socket> sockets = users.getSockets(nicks);

			// Envia para todo mundo da sala que um novo jogador entrou.
			new Sender(sockets).sendMessage(roomResponse.getResponseMessage());

			// Atualiza a sala na sessão.
			session.put(Session.KEY_ROOM, room);

		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void startGame() {
		try {
			Session session = Session.getInstance();

			// Recuperando a tabela de usuários.
			UsersTable users = (UsersTable) session
					.get(Session.KEY_USERS_LOGGED);

			// Recuperando a sala da sessão.
			Room room = (Room) session.get(Session.KEY_ROOM);

			// Verificando integridade da sala.
			if (room == null) {
				// TODO: enviar erro.
			}

			Player player1 = room.getJogador1();
			Player player2 = room.getJogador2();
			Player player3 = room.getJogador3();
			Player player4 = room.getJogador4();

			// Adiciona o jogador na próxima posição se possível.
			Game game = new Game(room);

			ArrayList<Hexagon> hexagons = game.initialConfiguration();

			GameResponse gameResponse = new GameResponse();
			if (player1 != null)
				gameResponse.setPlayer1(player1);
			if (player2 != null)
				gameResponse.setPlayer2(player2);
			if (player3 != null)
				gameResponse.setPlayer3(player3);
			if (player4 != null)
				gameResponse.setPlayer4(player4);

			// Alterando a configuração inicial do hexágonos.
			gameResponse.setHexagons(hexagons);

			// Indicando o jogador da vez.
			gameResponse.setPlayerTurn(game.getPlayerTurn());

			ArrayList<String> nicks = room.playersNick();
			ArrayList<Socket> sockets = users.getSockets(nicks);

			// Envia para todo mundo da sala que um novo jogador entrou.
			new Sender(sockets).sendMessage(gameResponse.getResponseMessage());

			// Atualiza a sala na sessão.
			session.put(Session.KEY_GAME, game);

		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
