package br.com.catan.protocol.request;

public class RoomRequest {

	public static final Object TYPE_NAME = "RoomRequest";
	
	private int action;

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}
}
