package br.com.catan.protocol.process;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import br.com.catan.model.UserLogged;
import br.com.catan.model.UsersTable;
import br.com.catan.protocol.ResponseCode;
import br.com.catan.protocol.Sender;
import br.com.catan.protocol.request.LoginRequest;
import br.com.catan.protocol.response.LoginResponse;
import br.com.catan.util.Session;

public class LoginProcess {

	private LoginRequest loginRequest;
	private Socket socket;

	public LoginProcess(LoginRequest l, Socket socket) {
		this.loginRequest = l;
		this.socket = socket;
	}

	public void execute() {
		try {
			Session session = Session.getInstance();
			String nick = loginRequest.getNick();

			// Coletando a lista de usuários logados.
			UsersTable listaUsuariosLogados = (UsersTable) session
					.get(Session.KEY_USERS_LOGGED);

			// Verificando integridade da lista de usuários logados.
			if (listaUsuariosLogados == null) {
				listaUsuariosLogados = new UsersTable();
			}

			// Criando o usuário.
			UserLogged user = new UserLogged();
			user.setNick(nick);
			user.setSocket(socket);

			// Criando a resposta de login.
			LoginResponse loginResponse = new LoginResponse();

			// Verificando se o usuário está na sessão.
			if (listaUsuariosLogados.containsKey(nick)) {
				loginResponse.setStatus(ResponseCode.LOGIN_USER_ALREADY_LOGGED
						.getCode());
			} else {
				// Caso o usuário não esteja logado, adicionar o usuário na
				// sessão.
				listaUsuariosLogados.put(nick, user);
				session.put(Session.KEY_USERS_LOGGED, listaUsuariosLogados);

				loginResponse.setStatus(ResponseCode.SUCCESS.getCode());
			}

			// Send message to the current socket.
			new Sender(socket).sendMessage(loginResponse.getResponseMessage());

		} catch (JsonGenerationException e) {

			// Register the exception on log
			Logger.getLogger(LoginProcess.class.getName()).log(Level.SEVERE,
					null, e);

		} catch (JsonMappingException e) {

			// Register the exception on log
			Logger.getLogger(LoginProcess.class.getName()).log(Level.SEVERE,
					null, e);

		} catch (IOException e) {

			// Register the exception on log
			Logger.getLogger(LoginProcess.class.getName()).log(Level.SEVERE,
					null, e);
		}

	}

}
