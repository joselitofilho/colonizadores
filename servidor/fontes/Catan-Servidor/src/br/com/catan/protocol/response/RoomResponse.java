package br.com.catan.protocol.response;

import br.com.catan.model.Player;

/**
 * Class that encapsulate the room response
 * 
 * @author Joselito
 * 
 */
public class RoomResponse extends AbstractResponse {

	/**
	 * Create the response of login protocol
	 */
	public RoomResponse() {

		super(RoomResponse.class.getSimpleName());

	}

	public void setPlayer1(Player player) {

		data.put("player1", player);

	}

	public void setPlayer2(Player player) {

		data.put("player2", player);

	}

	public void setPlayer3(Player player) {

		data.put("player3", player);

	}

	public void setPlayer4(Player player) {

		data.put("player4", player);

	}

}
