package br.com.catan.util;

import java.util.HashMap;

public class Session extends HashMap<String, Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5627131810986878928L;
	
	public static final String KEY_USERS_LOGGED = "usuarios_logados";
	public static final String KEY_ROOM = "room";
	public static final String KEY_GAME = "game";
	
	private static Session instance;
	
	private Session()
	{
		super();
	}
	
	public static Session getInstance() {
		if (instance == null) instance = new Session();
		
		return instance;
	}

	
	
}
