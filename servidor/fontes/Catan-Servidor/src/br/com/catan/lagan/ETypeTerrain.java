package br.com.catan.lagan;

public enum ETypeTerrain {

	FOREST(1), // madeira
	HILL(2), // tijolos
	FARMING(3), // Trigo
	MOUNTAIN(4), // Minério
	PASTURE(5), // Ovelha
	DESERT(6); // Deserto
	
	private int value;
	
	ETypeTerrain(int value) {
		this.value = value;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getValue() {
		return this.value;
	}
}
