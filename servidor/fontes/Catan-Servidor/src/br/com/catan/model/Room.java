package br.com.catan.model;

import java.util.ArrayList;

public class Room {

	public Player jogador1;
	public Player jogador2;
	public Player jogador3;
	public Player jogador4;
	
	public Room() {
		jogador1 = null;
		jogador2 = null;
		jogador3 = null;
		jogador4 = null;
	}
	
	public boolean adicionarJogador(String nick) {
		boolean adicionou = false;
		Player player = new Player();
		player.setNick(nick);
		
		if (jogador1 == null) {
			jogador1 = player;
			adicionou = true;
		} else if (jogador2 == null) {
			jogador2 = player;
			adicionou = true;
		} else if (jogador3 == null) {
			jogador3 = player;
			adicionou = true;
		} else if (jogador4 == null) {
			jogador4 = player;
			adicionou = true;
		}
		
		return adicionou;
	}

	public ArrayList<String> playersNick() {
		ArrayList<String> retorno = new ArrayList<String>();
		
		if (jogador1 != null) retorno.add(jogador1.getNick());
		if (jogador2 != null) retorno.add(jogador2.getNick());
		if (jogador3 != null) retorno.add(jogador3.getNick());
		if (jogador4 != null) retorno.add(jogador4.getNick());
		
		return retorno; 
	}

	public Player getJogador1() {
		return jogador1;
	}

	public Player getJogador2() {
		return jogador2;
	}

	public Player getJogador3() {
		return jogador3;
	}

	public Player getJogador4() {
		return jogador4;
	}

	/**
	 * 
	 * @return
	 */
	public int getNumberPlayer() {
		int numberPlayers = 0;
		
		if (jogador1 != null) {
			++numberPlayers;
		} else if (jogador2 == null) {
			++numberPlayers;
		} else if (jogador3 == null) {
			++numberPlayers;
		} else if (jogador4 == null) {
			++numberPlayers;
		}
		
		return numberPlayers;
	}
}
