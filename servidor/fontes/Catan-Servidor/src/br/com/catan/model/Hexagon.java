package br.com.catan.model;

import br.com.catan.lagan.ETypeTerrain;

public class Hexagon {

	private int typeTerrain;
	private int valueDice;
	private boolean hasThief;
	
	public int getTypeTerrain() {
		return typeTerrain;
	}
	public void setTypeTerrain(int typeTerrain) {
		this.typeTerrain = typeTerrain;
	}
	public int getValueDice() {
		return valueDice;
	}
	public void setValueDice(int valueDice) {
		this.valueDice = valueDice;
	}
	public boolean isHasThief() {
		return hasThief;
	}
	public void setHasThief(boolean hasThief) {
		this.hasThief = hasThief;
	}
}
