package br.com.catan.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import br.com.catan.lagan.ETypeTerrain;

public class Game {

	private Room room;
	private ArrayList<Hexagon> board;
	private Player playerTurn;

	public Game(Room room) {
		this.room = room;
		this.board = new ArrayList<Hexagon>();

		startGame();
		
		escolherJogadorInicial();
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Hexagon> initialConfiguration() {
		return board;
	}
	
	/**
	 * 
	 */
	private void startGame() {
		//
		ArrayList<ETypeTerrain> randomTypeTerrais = randomTypeTerrains();
		//
		ArrayList<Integer> randomDices = randomDices();
		// 
		board = hexagonsPossible();

		// TODO: Aleatório para os portos.
		
		for (int i=0; i < 19; i++) {
			// Terrains
			board.get(i).setTypeTerrain(randomTypeTerrais.get(i).getValue());
			
			// Has thief
			if (randomTypeTerrais.get(i) == ETypeTerrain.DESERT) {
				board.get(i).setHasThief(true);
			} else {
				board.get(i).setHasThief(false);
			}
		}
		
		for (int i=0; i < 18; i++) {
			// Dices
			if (board.get(i).getTypeTerrain() == ETypeTerrain.DESERT.getValue()) {
				board.get(i).setValueDice(randomDices.get(i));
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	private ArrayList<Integer> randomDices() {
		ArrayList<Integer> possiveisValores = new ArrayList<Integer>();

		possiveisValores.add(2);
		
		for (int i = 3; i <= 11; i++) {
			if (i != 7) {
				possiveisValores.add(i);
				possiveisValores.add(i);
			}
		}
		
		possiveisValores.add(12);

		// Só para garantir que estará embaralhado.
		Collections.shuffle(possiveisValores);
		Collections.shuffle(possiveisValores);
		Collections.shuffle(possiveisValores);

		return possiveisValores;
	}

	/**
	 * 
	 * @return
	 */
	private ArrayList<ETypeTerrain> randomTypeTerrains() {
		ArrayList<ETypeTerrain> possiveisValores = new ArrayList<ETypeTerrain>();

		// Inicializar tijolo e minério.
		for (int i = 1; i <= 3 /* total de hexágonos */; i++) {
			possiveisValores.add(ETypeTerrain.HILL);
			possiveisValores.add(ETypeTerrain.MOUNTAIN);
		}

		// Inicializar trigo, ovelha e madeira.
		for (int i = 1; i <= 4 /* total de hexágonos */; i++) {
			possiveisValores.add(ETypeTerrain.FARMING);
			possiveisValores.add(ETypeTerrain.PASTURE);
			possiveisValores.add(ETypeTerrain.FOREST);
		}

		possiveisValores.add(ETypeTerrain.DESERT);

		// Só para garantir que estará embaralhado.
		Collections.shuffle(possiveisValores);
		Collections.shuffle(possiveisValores);
		Collections.shuffle(possiveisValores);

		return possiveisValores;
	}

	/**
	 * 
	 * @return
	 */
	private ArrayList<Hexagon> hexagonsPossible() {
		ArrayList<Hexagon> ret = new ArrayList<Hexagon>();

		for (int position = 1; position <= 19; position++) {
			ret.add(new Hexagon());
		}
		
		return ret;
	}
	
	/**
	 * 
	 */
	private void escolherJogadorInicial() {
		int numberPlayers = room.getNumberPlayer();
		
		int randomPlayer = new Random().nextInt(numberPlayers) + 1;
		
		if (randomPlayer == 1) playerTurn = room.jogador1;
		else if (randomPlayer == 2) playerTurn = room.jogador2;
		else if (randomPlayer == 3) playerTurn = room.jogador3;
		else if (randomPlayer == 4) playerTurn = room.jogador4;
	}

	/**
	 * 
	 * @return
	 */
	public Player getPlayerTurn() {
		return playerTurn;
	}

}
