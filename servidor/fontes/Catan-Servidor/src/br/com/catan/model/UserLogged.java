package br.com.catan.model;

import java.net.Socket;

public class UserLogged {

	private String nick;
	private Socket socket;

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return nick.equals(((UserLogged)obj).getNick());
	}
	
}
