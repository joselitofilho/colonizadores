package br.com.catan.model;

import java.lang.reflect.Array;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsersTable extends HashMap<String, UserLogged> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Procura o <i>nick</i> do jogador que corresponde ao socket especificado.
	 * @param socket Socket no qual será feito a pesquisa do nick.
	 * @return Retorna <i>null</i> se não encontrar o nick correspondente ao socket especificado. 
	 */
	public String getNick(Socket socket) {

		Logger.getLogger(UsersTable.class.getName()).log(Level.INFO, "get nick of socket " + socket);

		Iterator<String> it = this.keySet().iterator();

		for (UserLogged user : this.values()) {

			if (socket.equals(user.getSocket())) {

				return it.next();

			} else {

				it.next();

			}

		}

		return null;

	}
	
	public ArrayList<Socket> getSockets(ArrayList<String> nicks) {
		ArrayList<Socket> sockets = new ArrayList<Socket>();

		Iterator<String> it = this.keySet().iterator();

		for (UserLogged user : this.values()) {

			sockets.add(user.getSocket());
		}
		
		return sockets;
	}
}
